int8_t ISZ_MAGIC[] = {0x49, 0x73, 0x5a, 0x21}; // "IsZ!" on x86

typedef struct isz_header_st {
  int8_t signature[4];	//00-03, 'IsZ!'
  uint8_t header_sz;	//04   , header size in bytes
  int8_t version;		//05   , version number

  uint32_t vsn;		//06-09, volume serial number
    
  uint16_t sector_sz;	//10-11, sector size in bytes
  uint32_t total_sectors;//12-15, total sectors of ISO image

  int8_t has_password;	//16   , is Password protected?

  int64_t segment_sz;	//17-24, size of segments in bytes
  uint32_t nblocks;		//25-28, number of chunks in image
  uint32_t block_sz;	//29-32, chunk size in bytes (must be multiple of sector_size)
  uint8_t block_ptr_sz;	//33   , chunk pointer length

  int8_t segment_num;	//34   , segment number of this segment file, max 99

  uint32_t chunk_offset;	//35-38, offset of chunk pointers, zero = none
  uint32_t segment_offset;	//39-42, offset of segment pointers, zero = none
  uint32_t data_offset;	//43-46, data offset
  int8_t reserved;		//47	  , 

  // next byte at 48 = size, size = 64 unpacked
}__attribute__ ((packed)) isz_header ;

//contents of 'has_password'
#define ADI_PLAIN       0        // no encryption
#define ADI_PASSWORD    1        // password protected (not used)
#define ADI_AES128      2        // AES128 encryption
#define ADI_AES192      3        // AES192 encryption
#define ADI_AES256      4        // AES256 encryption

// if 'segment_offset' != 0
typedef struct isz_seg_st {
  int64_t size;                  // segment size in bytes
  int32_t num_chks;                  // number of chunks in current file
  int32_t first_chkno;               // first chunk number in current file
  int32_t chk_off;                   // offset to first chunk in current file
  int32_t left_size;                 // uncompltete chunk bytes in next file
} isz_seg;

// if 'chunk_offset' != 0
/*
typedef struct isz_chunk_st {
  chunk_flag;
  blk_len;
} isz_chunk;
*/

// chunk_flag
#define ADI_ZERO        0x00    // all zeros chunk
#define ADI_DATA        0x40    // non-compressed data
#define ADI_ZLIB        0x80    // ZLIB compressed
#define ADI_BZ2         0xC0    // BZIP2 compressed





