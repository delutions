#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <inttypes.h>
#include "isz.h"

char * progname;

void usage(void) {
	printf("isz converter\n");
	printf("usage:\n\t%s file.isz\n",progname);
	exit(EXIT_FAILURE);
}

void print_isz_header(isz_header *header) {
	printf("Header Size : %u\n",header->header_sz);
	printf("Version : %u\n",header->version);
	printf("Volume Serial : %u\n",header->vsn);
	printf("Sector Size : %d\n",	header->sector_sz);
	printf("Total Sectors : %d\n",header->total_sectors);
	printf("Calculated Size : %d Bytes\n", (header->sector_sz) * (header->total_sectors) );

	printf("Password Flag : %d\n", header->has_password);
	printf("Segment Size : %ld\n", header->segment_sz);
	printf("Num Blocks/Chunks : %d\n", header->nblocks);
	printf("Block/Chunk Size : %d\n", header->block_sz);

	printf("Chunk Ptr len : %d\n", header->block_ptr_sz);
	printf("Segment Number : %d\n", header->segment_num);

	printf("Chunk Ptr Offset : %d\n", header->chunk_offset);
	printf("Segment Offset : %d\n", header->segment_offset);
	printf("Data Offset : %d\n", header->data_offset);

	printf("Reserved : %d\n",header->reserved);
}

void pread( void * ptr, size_t size, size_t count, FILE * stream ) {
	size_t s = fread(ptr,size,count,stream);
	if (s < (size*count)) {
		fprintf(stderr," read too little\n");
	}
}

int main (int argc, char ** argv) {
	progname = argv[0];

	fprintf(stderr," isz struct sz: 0x%X, %u\n",
		(unsigned int)sizeof(isz_header),(unsigned int)sizeof(isz_header));

	if (argc < 2) {
		usage();
	}

	isz_header * header = malloc(sizeof(isz_header));
	if (header == NULL) {
		fputs(" malloc error\n",stderr);
		exit(EXIT_FAILURE);
	}

	FILE * isz = fopen(argv[1],"rb");
	if (isz == NULL) {
		fprintf(stderr," could not open %s\n",argv[1]);
		exit(EXIT_FAILURE);
	}
	
	pread(header,1,sizeof(isz_header),isz);

	if ( memcmp( header->signature, ISZ_MAGIC, 
	  sizeof(ISZ_MAGIC)) ) {
		fputs(" Invalid Magic.\n",stderr);
		exit(EXIT_FAILURE);
	}

	if (header->header_sz != sizeof(isz_header) ) {
		fprintf(stderr, " Failure: ISZ file header size incorrect (is %lu, should be %lu)\n"
			" Possibly older isz version, unsupported\n",
			(long unsigned int)(header->header_sz), sizeof(isz_header));		
		//exit(EXIT_FAILURE);
	}

	print_isz_header(header);

	exit(EXIT_SUCCESS);
}
