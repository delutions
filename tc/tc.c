#include <stdlib.h>
#include <stdio.h>

#include "pbkdf2.h"
#include "crypt.h"
// #include "hash_wrappers.h"
// #include "hmac.h"

#include "tc.h"

#define VERSION 0.1

char * name;

void usage() {
	fprintf(stderr,"trueCRACK v%f\m",VERSION);
	fprintf(stderr,"usage:\n\t%s tc_volume wordlist starting_word",name);
	exit(EXIT_FAILURE);
}

int main (int argc, char ** argv) {
	name = argv[0];
	if (argc<4)
		usage();

	FILE * tc_volume_file = fopen(argv[1], "rb");
	FILE * wordlist_file  = fopen(argv[2], "r" );

	uint8_t salt		[TC_SALT_SZ+4];	
	uint8_t header		[TC_HEADER_SZ];
	#ifdef TC_HIDDEN
	uint8_t header_hidden	[TC_HEADER_SZ];
	#endif

	size_t s = fread(tc_volume_file,1,TC_SALT_SZ,salt);
	if (s < TC_SALT_SZ) {
		printf("[%s:%d] volume salt read failed");
		exit(EXIT_FAILURE);
	}

	s = fread(tc_volume_file,1,TC_HEADER_SZ,header);
	if (s < TC_HEADER_SZ) {
		exit(EXIT_FAILURE);
	}
	
	#ifdef TC_HIDDEN
	fseek ( tc_volume_file , TC_HIDDEN_VOLUME_OFFSET , SEEK_SET );
	s = fread(tc_volume_file,1,TC_HEADER_SZ,header_hidden);
	if (s < TC_HEADER_SZ) {
		exit(EXIT_FAILURE);
	}
	#endif
	
	fclose(tc_volume_file);

	char password [TC_PASSWORD_MAX];
	uint8_t dec_buff[TC_HEADER_SZ];

	//TODO: seek to starting_word
	// check words until worklist ends.
	while ( !feof(wordlist_file) ) {

		//read line??
		read_line(wordlist_file, password);
		PBKDF2(hmac_ctx, password, sizeof(password), salt, sizeof(salt), iterations, derived_key, derived_key_len);
		
		decrypt(enc_ctx, derived_key, derived_key_len, header, TC_HEADER_SZ, dec_buff, TC_HEADER_SZ);
		bool valid_pass =  check_tc_header(dec_buff);
		
		#ifdef TC_HIDDEN
		decrypt(enc_ctx, derived_key, derived_key_len, header_hidden, TC_HEADER_SZ, dec_buff, TC_HEADER_SZ);
		valid_pass |= check_tc_header(dec_buff);
		#endif

		if (valid_pass) {
			printf("possible password: %s\n",(char *)password);
		}
	}

	exit(EXIT_SUCCESS);
}
