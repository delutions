/* 
5.2 PBKDF2

   PBKDF2 applies a pseudorandom function (see Appendix B.1 for an
   example) to derive keys. The length of the derived key is essentially
   unbounded. (However, the maximum effective search space for the
   derived key may be limited by the structure of the underlying
   pseudorandom function. See Appendix B.1 for further discussion.)
   PBKDF2 is recommended for new applications.

   Note. The construction of the function F follows a "belt-and-
   suspenders" approach. The iterates U_i are computed recursively to
   remove a degree of parallelism from an opponent; they are exclusive-
   ored together to reduce concerns about the recursion degenerating
   into a small set of values.
*/


#include <stdint.h>
#include <math.h>

#include "crypt.h"


/*
   PBKDF2 (P, S, c, dkLen)

   Options:        PRF        underlying pseudorandom function (hLen
                              denotes the length in octets of the
                              pseudorandom function output)

   Input:          P          password, an octet string
                   S          salt, an octet string
                   c          iteration count, a positive integer
                   dkLen      intended length in octets of the derived
                              key, a positive integer, at most
                              (2^32 - 1) * hLen

   Output:         DK         derived key, a dkLen-octet string
*/
// Param
//	prf : in
//	password : in
//	password_len : in
// 	salt : no spare bytes required, in
//	salt_len : in
//	iterations : in
//	derived_key : out
//	derived_key_len : in
int PBKDF2(const hash_t * prf, uint8_t * password, uint32_t password_len, uint8_t * salt, uint32_t salt_len, uint32_t iterations, uint8_t * derived_key, uint32_t derived_key_len) {

	salt = realloc(salt, salt_len + 4); // Needed in the calls to 'F'

	// 1. If dkLen > (2^32 - 1) * hLen, output "derived key too long" and stop.
	if (derived_key_len > ( (pow(2,32)-1) * prf->digest_sz ) {
		return -1;

	// 2. Let l be the number of hLen-octet blocks in the derived key,
	//    rounding up, and let r be the number of octets in the last block:
        //       l = CEIL (dkLen / hLen) ,
        //       r = dkLen - (l - 1) * hLen .
	int l = ceil( derived_key_len / prf->digest_sz );
        int r = derived_key_len - (l - 1) * prf->digest_sz;

	
	// 3. For each block of the derived key apply the function F defined
        //    below to the password P, the salt S, the iteration count c, and
        //    the block index to compute the block:
	//
        //           T_1 = F (P, S, c, 1) ,
        //           T_2 = F (P, S, c, 2) ,
        //           ...
        //           T_l = F (P, S, c, l) ,
	// 4. Concatenate the blocks and extract the first dkLen octets to
        // produce a derived key DK:
	//
	//           DK = T_1 || T_2 ||  ...  || T_l<0..r-1>
	for (uint32_t block_num = 1; block_num <= l; block_num++) {
		uint8_t * block = derived_key[ hash->digest_sz * ( block_num-1 ) ];
		F(prf, password, password_len, salt, salt_len, iterations, block_num , block );
	}
	return 0;
}

/*
	 where the function F is defined as the exclusive-or sum of the
         first c iterates of the underlying pseudorandom function PRF
         applied to the password P and the concatenation of the salt S
         and the block index i:

		F (P, S, c, i) = U_1 \xor U_2 \xor ... \xor U_c
*/
// Param
//	prf : in
//	password : in
//	password_len : in
//	salt : needs 4 spare bytes
//	salt_len : the length of the real salt
//	iterations : in
//	block_index : in
//	block : out, length = prf->digest_sz
//	
inline void F(const hash_t * prf, uint8_t * password, uint32_t password_len, uint8_t * salt, uint32_t salt_len, uint32_t iterations, uint32_t block_index, uint8_t * block) {

	uint8_t U_new [hash->digest_sz];

	// U_1 = PRF (P, S || INT (i)) ,
	//         Here, INT (i) is a four-octet encoding of the integer i, most
	//         significant octet first.

	salt[salt_len  ] = (uint8_t)( ( block_index >> (8*3) ) & 0xFF);
	salt[salt_len+1] = (uint8_t)( ( block_index >> (8*2) ) & 0xFF);	
	salt[salt_len+2] = (uint8_t)( ( block_index >> ( 8 ) ) & 0xFF);
	salt[salt_len+3] = (uint8_t)( ( block_index          ) & 0xFF);

	prf->digest(password, password_len, salt, salt_len+4, block);

	// U_2 = PRF (P, U_1) ,
        // ...
        // U_c = PRF (P, U_{c-1}) .
	for(uint32_t i = 1; i < iterations; i++) {
		prf->digest( password, password_len, block, prf->digest_sz, U_new);

		// FIXME: use a 32 or 64 bit xor??
		for (uint32_t i = 0; i < len; i ++)
			block[i] = block[i] ^ U_new[i];
		
	}
}

