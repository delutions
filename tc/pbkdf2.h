

int PBKDF2(const hash_t * hash, uint8_t * password, uint32_t password_len, uint8_t * salt, uint32_t salt_len, uint32_t iterations, uint8_t * derived_key, uint32_t derived_key_len);


