/**********************************************
**
**
**	Defines for PhnxSoft
**
**
***********************************************/

#include	<stdio.h>
#include	"./phnxver.h"

#define         SftName         "PhoenixDeco"
#define         SftEMail        "Anton Borisov, anton.borisov@gmail.com"

        /********SoftWare*************/                                         
        byte SoftName[]         = "-="SftName", version "SftVersion"=-";
        byte CopyRights[]       = "\n(C) Anton Borisov, 2000, 2002-2004, Portions (C) 1999-2000";
	byte Url[]              = "Bug-reports direct to "SftEMail;
